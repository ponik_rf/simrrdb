/*
 * Copyright © 2023 Boris Bobylev. All rights reserved.
 * Licensed under the Apache License, Version 2.0
*/


import net from 'net'
import { Database } from 'vrack-db';

export default class GraphiteReceiver {
    shares = {
        clients: 0,
        received: 0
    }

    index = 0;

    buffer: {[key:number]: string} = {}
    timers: {[key:number]: NodeJS.Timeout} = {}

    db: Database

    constructor(db: Database){
        this.db = db
    }

    init(host: string, port: number) {
        const server = net.createServer((socket) => {
            this.shares.clients++
            const sid = this.getNewId()
            socket.on('close', () => {
                this.shares.clients--
                delete this.buffer[sid]
                delete this.timers[sid]
            })
            socket.on('data', (data) => {
                const strData = data.toString('utf8')
                if (strData.slice(-1) !== '\n') return this.updateBuffer(sid, strData)
                const rows = strData.split('\n')
                for (const row of rows) {
                    const acts = row.split(' ')
                    if (acts.length === 3) {
                        this.shares.received++
                        this.db.write( acts[0], parseFloat(acts[1]),parseInt(acts[2]))
                    } else {
                        continue
                    }
                }
                this.clearBuffer(sid)
            })
        })

        server.listen(port, host)
    }

    clearBuffer(id:number) {
        this.buffer[id] = ''
    }

    updateBuffer(id: number, data: string) {
        this.buffer[id] = this.buffer[id] + data
        this.bufferTimer(id)
    }

    bufferTimer(id: number) {
        if (this.timers[id]) clearTimeout(this.timers[id])
        this.timers[id] = setTimeout(() => {
            this.buffer[id] = ''
            delete this.timers[id]
        }, 3000)
    }

    getNewId() {
        return ++this.index
    }

}