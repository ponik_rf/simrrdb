/*
 * Copyright © 2023 Boris Bobylev. All rights reserved.
 * Licensed under the Apache License, Version 2.0
*/


import { Database } from 'vrack-db';
import express, { NextFunction, Request, Response } from 'express'
import querystring from 'node:querystring';

interface IDataPoints {
    target: string,
    datapoints: Array<Array<number | null>>
}

export default class WebApi {
    db: Database
    constructor(db: Database) {
        this.db = db
    }

    init(port: number) {
        const app = express();
        app.use(this.debug.bind(this));
        app.post('/render', this.render.bind(this))
        app.post('/metrics/find', this.find.bind(this))
        app.listen(port, () => {
            console.log(`[server]: Server is running at http://localhost:${port}`);
        });
    }

    render(req: Request, res: Response, next: NextFunction) {
        const urlBody: any = querystring.decode(req.body)
        if (urlBody.target && typeof urlBody.target === 'string') {
            const result: IDataPoints = { target: urlBody.target, datapoints: [] }
            const start = parseInt(urlBody.from)
            const end = parseInt(urlBody.until)
            let prec = Math.floor((end - start) / 300)
            if (prec === 0) prec = 1
            if (isNaN(prec)) { return res.send([]) }
            const dbRes = this.db.readCustomRange(urlBody.target, start, end, `${prec}s`)
            for (const row of dbRes.rows) result.datapoints.push([row.value, row.time])
            return res.send([result])
        }
        res.send([])
    }

    find(req: Request, res: Response, next: NextFunction) {
        let query = '*'
        const urlBody = querystring.decode(req.body)
        if (urlBody.query && typeof urlBody.query === 'string'){
          query = urlBody.query
        }
        const result: Array<{[index: string] : string | number}> = []
        const finds = this.db.MetricTree.find(query)
        for (const find of finds){
          const newRes: { [index: string] : string | number} = {
            text: find.name,
            expandable: (find.leaf)?0:1,
            leaf:  (find.leaf)?1:0,
            id: find.path,
            allowChildren: (find.leaf)?0:1,
          }
          result.push(newRes)
        }
        res.send(result);
    }


    debug(req: Request, res: Response, next: NextFunction) {
        // console.log('--------------------------') 
        // console.log(req)
        // console.log(req.headers)
        // console.log(req.method)
        // console.log(req.url)
        // console.log(req.query)
        // console.log('params:',req.params)
        // console.log('body:', req.body)
        // console.log('--------------------------')
        let data = '';
        req.setEncoding('utf8');
        req.on('data', function (chunk) { data += chunk; });
        req.on('end', function () { req.body = data; next(); });
    }
}