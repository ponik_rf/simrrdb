/*
 * Copyright © 2023 Boris Bobylev. All rights reserved.
 * Licensed under the Apache License, Version 2.0
*/


import { readFileSync } from "fs";
import path from "path";
import { Database } from "vrack-db";
import WebApi from "./WebApi";
import GraphiteReceiver from "./GraphiteReceiver";


const addSchemes = JSON.parse(readFileSync(path.join(process.cwd(), 'schemes.json')).toString('utf-8'))
const config = JSON.parse(readFileSync(path.join(process.cwd(), 'config.json')).toString('utf-8'))

const DB = new Database({ metricTree:true })

for (const sch of addSchemes) DB.scheme(sch.name, sch.pattern, sch.retentions)

const nApi = new WebApi(DB)
nApi.init(config.apiPort)

const nGR = new GraphiteReceiver(DB)
nGR.init(config.metricHost, config.metricPort)

