Simrrdb
=======

![graph](/docs/sin.png)

[RUSSIAN VERSION](/docs/RU/README.md)

Simrrdb - Simple in memory round-robin database. (TypeScript)
A very simple and minimalistic database that can act as a Graphite database for Grafana. 

The database only supports:

 - Retrieving Graphite metrics - `metric_path value timestamp\n` on port 2003 (default).
 - Finding metrics in Grafana 
 - Output metrics in Grafana

**That is, Graphite's additional math functions are not supported (aggregation, etc.).

What the database can be used for:

 - To quickly get started with Grafana without using outdated Graphite 
 - Demo data presentations - when you need to demonstrate data acquisition but don't need to store it.
 - Graphing without storing data on SSD/MMC/SD
  
Installation 
---------

Until the npm package is ready, you can use *gitlab*.

```
git clone https://gitlab.com/ponik_rf/simrrdb.git
```

Go to the project folder and install the dependencies

```
npm install
```

Next we need to rename the configuration files


 - schemes.example.json -> schemes.json
 - config.example.json -> config.json 

After that we can run the database 

```
npm run start
```

Use
----

You can now create a Graphite Data Source in Graphite and specify the address of the server on which the database is running. Graphite's default port is 8100. 

When you test the DataSource you should get an OK response. 

**Be aware that the database was tested with Grafana 9.2.6 and it is not known how it will behave with other versions**.

Metrics can be written to the database in Graphite format on port 2003 (default).

#### Here's what the official documentation says about it:

Graphite understands messages with this format:

```
metric_path value timestamp\n
```

 - metric_path is the metric namespace you want to populate.
 - value is the value you want to assign to the metric at the moment.
 - timestamp is the number of seconds that have passed since the unix era.

So you need to create a socket and write metrics into it in a row separated by a line break

Example for linux console:

```
echo "test.bash.stats 42 `date +%s`" | nc graphite.example.com 2003
```

Customization
---------

Ports can be configured in the `config.json` file. Schemas are configured in the `schemes.json` file. 

You can read more about the schema format in the Graphite documentation or in the official database documentation[VRack-DB](https://gitlab.com/vrack/vrack-db).