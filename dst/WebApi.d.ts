import { Database } from 'vrack-db';
import express, { NextFunction, Request, Response } from 'express';
export default class WebApi {
    db: Database;
    constructor(db: Database);
    init(port: number): void;
    render(req: Request, res: Response, next: NextFunction): express.Response<any, Record<string, any>> | undefined;
    find(req: Request, res: Response, next: NextFunction): void;
    debug(req: Request, res: Response, next: NextFunction): void;
}
