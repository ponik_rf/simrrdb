/// <reference types="node" />
import { Database } from 'vrack-db';
export default class GraphiteReceiver {
    shares: {
        clients: number;
        received: number;
    };
    index: number;
    buffer: {
        [key: number]: string;
    };
    timers: {
        [key: number]: NodeJS.Timeout;
    };
    db: Database;
    constructor(db: Database);
    init(host: string, port: number): void;
    clearBuffer(id: number): void;
    updateBuffer(id: number, data: string): void;
    bufferTimer(id: number): void;
    getNewId(): number;
}
