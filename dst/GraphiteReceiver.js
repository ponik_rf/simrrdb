"use strict";
/*
 * Copyright © 2023 Boris Bobylev. All rights reserved.
 * Licensed under the Apache License, Version 2.0
*/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const net_1 = __importDefault(require("net"));
class GraphiteReceiver {
    constructor(db) {
        this.shares = {
            clients: 0,
            received: 0
        };
        this.index = 0;
        this.buffer = {};
        this.timers = {};
        this.db = db;
    }
    init(host, port) {
        const server = net_1.default.createServer((socket) => {
            this.shares.clients++;
            const sid = this.getNewId();
            socket.on('close', () => {
                this.shares.clients--;
                delete this.buffer[sid];
                delete this.timers[sid];
            });
            socket.on('data', (data) => {
                const strData = data.toString('utf8');
                if (strData.slice(-1) !== '\n')
                    return this.updateBuffer(sid, strData);
                const rows = strData.split('\n');
                for (const row of rows) {
                    const acts = row.split(' ');
                    if (acts.length === 3) {
                        this.shares.received++;
                        this.db.write(acts[0], parseFloat(acts[1]), parseInt(acts[2]));
                    }
                    else {
                        continue;
                    }
                }
                this.clearBuffer(sid);
            });
        });
        server.listen(port, host);
    }
    clearBuffer(id) {
        this.buffer[id] = '';
    }
    updateBuffer(id, data) {
        this.buffer[id] = this.buffer[id] + data;
        this.bufferTimer(id);
    }
    bufferTimer(id) {
        if (this.timers[id])
            clearTimeout(this.timers[id]);
        this.timers[id] = setTimeout(() => {
            this.buffer[id] = '';
            delete this.timers[id];
        }, 3000);
    }
    getNewId() {
        return ++this.index;
    }
}
exports.default = GraphiteReceiver;
