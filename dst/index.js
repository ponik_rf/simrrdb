"use strict";
/*
 * Copyright © 2023 Boris Bobylev. All rights reserved.
 * Licensed under the Apache License, Version 2.0
*/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = __importDefault(require("path"));
const vrack_db_1 = require("vrack-db");
const WebApi_1 = __importDefault(require("./WebApi"));
const GraphiteReceiver_1 = __importDefault(require("./GraphiteReceiver"));
const addSchemes = JSON.parse((0, fs_1.readFileSync)(path_1.default.join(process.cwd(), 'schemes.json')).toString('utf-8'));
const config = JSON.parse((0, fs_1.readFileSync)(path_1.default.join(process.cwd(), 'config.json')).toString('utf-8'));
const DB = new vrack_db_1.Database({ metricTree: true });
for (const sch of addSchemes)
    DB.scheme(sch.name, sch.pattern, sch.retentions);
const nApi = new WebApi_1.default(DB);
nApi.init(config.apiPort);
const nGR = new GraphiteReceiver_1.default(DB);
nGR.init(config.metricHost, config.metricPort);
