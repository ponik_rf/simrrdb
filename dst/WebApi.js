"use strict";
/*
 * Copyright © 2023 Boris Bobylev. All rights reserved.
 * Licensed under the Apache License, Version 2.0
*/
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const node_querystring_1 = __importDefault(require("node:querystring"));
class WebApi {
    constructor(db) {
        this.db = db;
    }
    init(port) {
        const app = (0, express_1.default)();
        app.use(this.debug.bind(this));
        app.post('/render', this.render.bind(this));
        app.post('/metrics/find', this.find.bind(this));
        app.listen(port, () => {
            console.log(`[server]: Server is running at http://localhost:${port}`);
        });
    }
    render(req, res, next) {
        const urlBody = node_querystring_1.default.decode(req.body);
        if (urlBody.target && typeof urlBody.target === 'string') {
            const result = { target: urlBody.target, datapoints: [] };
            const start = parseInt(urlBody.from);
            const end = parseInt(urlBody.until);
            let prec = Math.floor((end - start) / 300);
            if (prec === 0)
                prec = 1;
            if (isNaN(prec)) {
                return res.send([]);
            }
            const dbRes = this.db.readCustomRange(urlBody.target, start, end, `${prec}s`);
            for (const row of dbRes.rows)
                result.datapoints.push([row.value, row.time]);
            return res.send([result]);
        }
        res.send([]);
    }
    find(req, res, next) {
        let query = '*';
        const urlBody = node_querystring_1.default.decode(req.body);
        if (urlBody.query && typeof urlBody.query === 'string') {
            query = urlBody.query;
        }
        const result = [];
        const finds = this.db.MetricTree.find(query);
        for (const find of finds) {
            const newRes = {
                text: find.name,
                expandable: (find.leaf) ? 0 : 1,
                leaf: (find.leaf) ? 1 : 0,
                id: find.path,
                allowChildren: (find.leaf) ? 0 : 1,
            };
            result.push(newRes);
        }
        res.send(result);
    }
    debug(req, res, next) {
        // console.log('--------------------------') 
        // console.log(req)
        // console.log(req.headers)
        // console.log(req.method)
        // console.log(req.url)
        // console.log(req.query)
        // console.log('params:',req.params)
        // console.log('body:', req.body)
        // console.log('--------------------------')
        let data = '';
        req.setEncoding('utf8');
        req.on('data', function (chunk) { data += chunk; });
        req.on('end', function () { req.body = data; next(); });
    }
}
exports.default = WebApi;
